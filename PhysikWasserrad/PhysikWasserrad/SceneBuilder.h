#pragma once
class SceneBuilder
{
public:
	SceneBuilder(PxPhysics* gPhysicsSDK, PxScene* gScene);
	~SceneBuilder(void);

	PxScene* gScene;
	PxPhysics* gPhysicsSDK;

	void RenderObjects();
	void DrawActor(PxRigidActor* actor);
	void DrawBoxActor(PxRigidActor* actor);

private:

	void getColumnMajor(PxMat33 m, PxVec3 t, float* mat);
	void DrawConvexMesh(PxShape* pShape, PxRigidActor* actor);
	void DrawShape(PxShape* shape, PxRigidActor* actor);
	

	void DrawGrid(int size);
	void DrawFeed();
	void DrawBasin();
	void CreateGroundPlane();
	void CreateFeed();
	void CreateBasin();

	WavefrontObj feedWavefront, basinWavefront;
	Model_OBJ feedOBJ, basinOBJ;
};

