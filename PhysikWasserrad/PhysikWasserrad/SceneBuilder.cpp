#include "stdafx.h"


SceneBuilder::SceneBuilder(PxPhysics* gPhysicsSDK, PxScene* gScene)
{
	this->gPhysicsSDK = gPhysicsSDK;
	this->gScene = gScene;

	CreateFeed();
	CreateBasin();
}


SceneBuilder::~SceneBuilder(void)
{
}


void SceneBuilder::DrawGrid(int size)
{
	glPushMatrix();
		glTranslatef(0.0f, -9.0f, 0.0f);
		glBegin(GL_LINES);
		glColor3f(0.7f, 0.7f, 0.7f);

		for(int i=-size;i<=size;i++)
		{
			glVertex3f((float)i,0,(float)-size);
			glVertex3f((float)i,0,(float)size);

			glVertex3f((float)-size,0,(float)i);
			glVertex3f((float)size,0,(float)i);
		}
		glEnd();
	glPopMatrix();
}

void SceneBuilder::CreateFeed()
{
    feedWavefront = WavefrontObj();
    unsigned int tris = feedWavefront.loadObj("feed.obj", false);
    feedOBJ.Load("feed.obj");

    PxCooking *cooker = PxCreateCooking(PX_PHYSICS_VERSION,gPhysicsSDK->getFoundation(),PxCookingParams(PxTolerancesScale()));

    PxTriangleMeshDesc meshDesc;
    meshDesc.points.count                = feedWavefront.mVertexCount;
    meshDesc.triangles.count            = feedWavefront.mTriCount;
    meshDesc.points.stride                = sizeof(float) * 3;
    meshDesc.triangles.stride            = sizeof(int) * 3;
    meshDesc.points.data                = feedWavefront.mVertices;
    meshDesc.triangles.data                = feedWavefront.mIndices;
    
    PxDefaultMemoryOutputStream buffer;

    bool success = cooker->cookTriangleMesh(meshDesc, buffer); 

    if(!success)
        cout<<"Error cooking"<<endl;

    PxTriangleMesh* triangleMesh = NULL;
    PxDefaultMemoryInputData readBuffer(buffer.getData(), buffer.getSize() );
    triangleMesh = gPhysicsSDK->createTriangleMesh(readBuffer);

    PxMaterial* mMaterial = gPhysicsSDK->createMaterial(0.5,0.5,0.5);
    PxTransform transform(PxVec3(0.0f, 5.0f, 1.0f), PxQuat::createIdentity());    
    PxReal density = 1.0f;

    PxRigidStatic* rigidStatic = PxCreateStatic(*gPhysicsSDK, transform, PxTriangleMeshGeometry(triangleMesh), *mMaterial);

    if(!rigidStatic)
        cout<<"Failed creating actor"<<endl;

    gScene->addActor(*rigidStatic);
}

void SceneBuilder::CreateBasin()
{
    basinWavefront = WavefrontObj();
    unsigned int tris = basinWavefront.loadObj("basin.obj", false);
    basinOBJ.Load("basin.obj");

    PxCooking *cooker = PxCreateCooking(PX_PHYSICS_VERSION,gPhysicsSDK->getFoundation(),PxCookingParams(PxTolerancesScale()));

    PxTriangleMeshDesc meshDesc;
    meshDesc.points.count                = basinWavefront.mVertexCount;
    meshDesc.triangles.count            = basinWavefront.mTriCount;
    meshDesc.points.stride                = sizeof(float) * 3;
    meshDesc.triangles.stride            = sizeof(int) * 3;
    meshDesc.points.data                = basinWavefront.mVertices;
    meshDesc.triangles.data                = basinWavefront.mIndices;
    
    PxDefaultMemoryOutputStream buffer;

    bool success = cooker->cookTriangleMesh(meshDesc, buffer); 

    if(!success)
        cout<<"Error cooking"<<endl;

    PxTriangleMesh* triangleMesh = NULL;
    PxDefaultMemoryInputData readBuffer(buffer.getData(), buffer.getSize() );
    triangleMesh = gPhysicsSDK->createTriangleMesh(readBuffer);

    PxMaterial* mMaterial = gPhysicsSDK->createMaterial(0.5,0.5,0.5);
    PxTransform transform(PxVec3(0.0f, -8.0f, 3.0f), PxQuat::createIdentity());    
    PxReal density = 1.0f;

    PxRigidStatic* rigidStatic = PxCreateStatic(*gPhysicsSDK, transform, PxTriangleMeshGeometry(triangleMesh), *mMaterial);

    if(!rigidStatic)
        cout<<"Failed creating actor"<<endl;

    gScene->addActor(*rigidStatic);
}

void SceneBuilder::DrawFeed()
{
	glPushMatrix();
     glEnableClientState(GL_VERTEX_ARRAY);                        // Enable vertex arrays
     glEnableClientState(GL_NORMAL_ARRAY);                        // Enable normal arrays
    glTranslatef(0.0f, 5.0f, 1.0f);
    glVertexPointer(3,GL_FLOAT,    0, feedOBJ.Faces_Triangles);                // Vertex Pointer to triangle array
    glNormalPointer(GL_FLOAT, 0, feedOBJ.normals);                        // Normal pointer to normal array
    glDrawArrays(GL_TRIANGLES, 0, feedOBJ.TotalConnectedTriangles);        // Draw the triangles
    glDisableClientState(GL_VERTEX_ARRAY);                        // Disable vertex arrays
    glDisableClientState(GL_NORMAL_ARRAY);      
	glPopMatrix();
}

void SceneBuilder::DrawBasin()
{
	glPushMatrix();
     glEnableClientState(GL_VERTEX_ARRAY);                        // Enable vertex arrays
     glEnableClientState(GL_NORMAL_ARRAY);                        // Enable normal arrays
    glTranslatef(0.0f, -8.0f, 3.0f);
    glVertexPointer(3,GL_FLOAT,    0, basinOBJ.Faces_Triangles);                // Vertex Pointer to triangle array
    glNormalPointer(GL_FLOAT, 0, basinOBJ.normals);                        // Normal pointer to normal array
    glDrawArrays(GL_TRIANGLES, 0, basinOBJ.TotalConnectedTriangles);        // Draw the triangles
    glDisableClientState(GL_VERTEX_ARRAY);                        // Disable vertex arrays
    glDisableClientState(GL_NORMAL_ARRAY);                        // Disable normal arrays
	glPopMatrix();
}


void SceneBuilder::CreateGroundPlane()
{
	PxMaterial* planeMaterial = gPhysicsSDK->createMaterial(0.9f, 0.1f, 1.0f);

	PxTransform pose = PxTransform(PxVec3(0.0f, 0.0f, 0.0f), PxQuat(PxHalfPi, PxVec3(0.0f, 0.2f, 1.0f)));
	PxRigidStatic* plane = gPhysicsSDK->createRigidStatic(pose);
	PxShape* shape = plane->createShape(PxPlaneGeometry(), *planeMaterial);
	gScene->addActor(*plane);
}

void SceneBuilder::DrawConvexMesh(PxShape* pShape, PxRigidActor* actor)
{
	// We cooked the actor with shapes out of PxConvexMeshGeometry.
	// We now know, that we need to display a PxConvexMeshGeometry.

	PxTransform pT = PxShapeExt::getGlobalPose(*pShape, *actor);
	PxConvexMeshGeometry cmg;
	PxTriangleMeshGeometry tg;
	pShape->getTriangleMeshGeometry(tg);
	pShape->getConvexMeshGeometry(cmg);		// Get convex mesh from shape
	PxMat33 m = PxMat33(pT.q );
	float mat[16];
	getColumnMajor(m,pT.p, mat);


	// Prepare convex mesh parts
	//PxConvexMesh* convexMesh = cmg.convexMesh;
	//PxU32 nbVerts = convexMesh->getNbVertices();
	//const PxVec3* convexVerts = convexMesh->getVertices();
	//const PxU8* indexBuffer = convexMesh->getIndexBuffer();

	//PxU32 nbPolygons = convexMesh->getNbPolygons();

	//PxU32 offset = 0;
  //  for(PxU32 i=0;i<nbPolygons;i++)
  //  {
		//PxHullPolygon face;
		//bool status = convexMesh->getPolygonData(i, face);
		//PX_ASSERT(status);

		//const PxU8* faceIndices = indexBuffer + face.mIndexBase;
		//for(PxU32 j=0;j<face.mNbVerts;j++)
		//{
		//	vertices[offset+j] = convexVerts[faceIndices[j]];
		//	normals[offset+j] = PxVec3(face.mPlane[0], face.mPlane[1], face.mPlane[2]);
		//}

		//for(PxU32 j=2;j<face.mNbVerts;j++)
		//{
		//	*triangles++ = PxU16(offset);
		//	*triangles++ = PxU16(offset+j);
		//	*triangles++ = PxU16(offset+j-1);
		//}

		//offset += face.mNbVerts;
  //  }


	// Draw each polygon (as Triangle?)
	glPushMatrix();
	glMultMatrixf(mat);
	glEnableClientState( GL_VERTEX_ARRAY );
	//glVertexPointer( 3, GL_FLOAT, 0, convexMesh->getVertices() );
	//glDrawElements( GL_TRIANGLES,
	//	3 *  nbPolygons,
	//	GL_UNSIGNED_SHORT,
	//	//HOW DO I ACCESS THE INDICES DATA!!!!!!!!!??????
	//);
	glVertexPointer(3, GL_FLOAT, 0, tg.triangleMesh->getVertices() );
	glDrawElements(GL_TRIANGLES, 3 * tg.triangleMesh->getNbTriangles() , GL_UNSIGNED_SHORT , tg.triangleMesh->getTriangles() );
	glDisableClientState( GL_VERTEX_ARRAY );
	glPopMatrix();
}

void SceneBuilder::DrawShape(PxShape* shape, PxRigidActor* actor)
{
	DrawConvexMesh(shape, actor);
}

void SceneBuilder::DrawActor(PxRigidActor* actor)
{
	PxU32 nShapes = actor->getNbShapes();
	PxShape** shapes = new PxShape*[nShapes];
	actor->getShapes(shapes, nShapes);
	while (nShapes--)
	{
		//DrawShape(shapes[nShapes], actor);
	}
	DrawShape(shapes[0], actor);

	delete [] shapes;
}

void SceneBuilder::DrawBoxActor(PxRigidActor* actor)
{
	PxU32 nShapes = actor->getNbShapes();
	PxShape** shapes = new PxShape*[nShapes];
	actor->getShapes(shapes, nShapes);
	
	while (nShapes--)
	{
		// Draw the shape
		PxTransform pT = PxShapeExt::getGlobalPose(*shapes[nShapes], *actor);
		PxMat33 m = PxMat33(pT.q );
		PxBoxGeometry bg;
		float mat[16];
		getColumnMajor(m, pT.p, mat);

		shapes[nShapes]->getBoxGeometry(bg);
		glPushMatrix();
		glMultMatrixf(mat);
		glScalef( bg.halfExtents.x*2, bg.halfExtents.y*2, bg.halfExtents.z*2 );		// Since it's a box, we know we have to use halfExtents
		glutSolidCube( 1 );
		glPopMatrix();
	}

	

	delete [] shapes;
}

void SceneBuilder::RenderObjects()
{
	DrawGrid(10);

	DrawFeed();
	DrawBasin();

	// Set actor shading & stage lighting

	GLfloat lightAmbientColour[] = { 0.4f, 0.4f, 0.4f, 1.0f };
	GLfloat lightDiffuseColour[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat lightSpecularColour[] = { 0.8f, 0.8f, 0.8f, 1.0f };

	// Stage Lighting
	GLfloat lightPosition[4] = {20*cos(0.0),20*sin(0.0),0.0,1.0};
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,lightAmbientColour);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,lightDiffuseColour);
	glLightfv(GL_LIGHT0,GL_SPECULAR,lightSpecularColour);
	glLightfv(GL_LIGHT0,GL_POSITION, lightPosition);

	// Actor Shading
	GLfloat ambient_cube[]={0.0f,0.0f,0.30f,0.40f};
	GLfloat ambient_sphere[]={0.20f,0.20f,0.0f,0.25f};

	GLfloat diffuse_cube[]={1.0f,1.0f,1.0f,1.0f};
	GLfloat diffuse_sphere[]={1.0f,1.0f,1.0f,1.0f};

	GLfloat mat_diffuse_cube[]={0.85f, 0.0f, 0.0f, 1.0f};
	GLfloat mat_diffuse_sphere[]={0.85f, 0.85f, 0.0f, 1.0f};

}

void SceneBuilder::getColumnMajor(PxMat33 m, PxVec3 t, float* mat)
{
	mat[0] = m.column0[0];
	mat[1] = m.column0[1];
	mat[2] = m.column0[2];
	mat[3] = 0;

	mat[4] = m.column1[0];
	mat[5] = m.column1[1];
	mat[6] = m.column1[2];
	mat[7] = 0;

	mat[8] = m.column2[0];
	mat[9] = m.column2[1];
	mat[10] = m.column2[2];
	mat[11] = 0;

	mat[12] = t[0];
	mat[13] = t[1];
	mat[14] = t[2];
	mat[15] = 1;
}