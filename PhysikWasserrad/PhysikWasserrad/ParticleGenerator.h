#pragma once
class ParticleGenerator
{
public:
	ParticleGenerator(PxPhysics* gPhysicsSDK, PxScene* gScene);
	~ParticleGenerator(void);
	void RenderActors();
	void CreateParticles(int num);

	vector <PxRigidActor*> spheres;

private:
	void getColumnMajor(PxMat33 m, PxVec3 t, float* mat);
	void DrawSphere(PxShape* pShape, PxRigidActor* actor);
	void DrawShape(PxShape* shape, PxRigidActor* actor);
	void DrawActor(PxRigidActor* actor);

private:
	PxScene* gScene;
	PxPhysics* gPhysicsSDK;

	GLfloat mat_diffuse_sphere[4];
	
};

