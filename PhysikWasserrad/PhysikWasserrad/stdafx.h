#pragma once
#include <stdio.h>
#include <iostream>
#include <vector>

#include <windows.h>
#include <fstream>
#include <string.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <sstream>
#include <string>
#include <cmath>
#include <time.h>

#include <PxPhysicsAPI.h>
#include <extensions\PxExtensionsAPI.h>
#include <extensions\PxDefaultErrorCallback.h>
#include <extensions\PxDefaultAllocator.h>
#include <extensions\PxDefaultSimulationFilterShader.h>
#include <extensions\PxDefaultCpuDispatcher.h>
#include <extensions\PxShapeExt.h>
#include <extensions\PxSimpleFactory.h>
#include <extensions\PxParticleExt.h>
#include <extensions\PxRigidBodyExt.h>
#include <foundation\PxFoundation.h>
#include <SampleBase\RenderBaseActor.h>
#include <SampleBase\PhysXSample.h>
#include <PxToolkit.h>
#include "wavefront.h"

using namespace physx;
using namespace std;

#include "Util.h"
#include "Model_OBJ.h"
#include "SceneBuilder.h"
#include "ParticleGenerator.h"



#pragma comment(lib, "PhysX3_x86.lib")
#pragma comment(lib, "PhysX3Common_x86.lib")
#pragma comment(lib, "PhysX3Extensions.lib")
#pragma comment(lib, "PxTask.lib")

