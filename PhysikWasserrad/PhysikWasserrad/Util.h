class Util
{
public:
	static int getRandomInt(int min, int max){

		return min + (rand() % (int)(max - min + 1));

	}

	static float getRandomFloat(float min, float max)
	{
		return min + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(max - min)));
	}

};