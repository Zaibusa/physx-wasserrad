#include "stdafx.h"

ParticleGenerator::ParticleGenerator(PxPhysics* gPhysicsSDK, PxScene* gScene)
{

	this->gPhysicsSDK = gPhysicsSDK;
	this->gScene = gScene;

	mat_diffuse_sphere[0] = 0.85f;
	mat_diffuse_sphere[1] = 0.85f;
	mat_diffuse_sphere[2] = 0.0f;
	mat_diffuse_sphere[3] = 1.0f;

	srand (static_cast <unsigned> (time(0)));
}



ParticleGenerator::~ParticleGenerator(void)
{

}

void ParticleGenerator::DrawSphere(PxShape* pShape, PxRigidActor* actor)
{
	PxTransform pT = PxShapeExt::getGlobalPose(*pShape, *actor);
	PxSphereGeometry bg;
	pShape->getSphereGeometry(bg);
	PxMat33 m = PxMat33(pT.q );
	float mat[16];
	getColumnMajor(m,pT.p, mat);
	glPushMatrix();
	glMultMatrixf(mat);
	//glutSolidSphere(bg.radius,3,3);
	glutSolidCube(bg.radius);
	glPopMatrix();
}

void ParticleGenerator::DrawShape(PxShape* shape, PxRigidActor* actor)
{
	DrawSphere(shape, actor);
}

void ParticleGenerator::DrawActor(PxRigidActor* actor)
{
	PxU32 nShapes = actor->getNbShapes();
	PxShape** shapes = new PxShape*[nShapes];
	actor->getShapes(shapes, nShapes);
	while (nShapes--)
	DrawShape(shapes[nShapes], actor);

	delete [] shapes;
}

void ParticleGenerator::CreateParticles(int num)
{
	PxMaterial* sphereMaterial = gPhysicsSDK->createMaterial(0.6f, 0.1f, 0.6f);

	// Initialize Sphere Actor
	PxReal sphereDensity = 2.0f;
	PxTransform sphereTransform(PxVec3(0.0f, 4.0f, 10.0f));
	PxSphereGeometry sphereGeometry(0.1f);
	for (int i=0; i<num; i++)
	{
		sphereTransform.p  = PxVec3(Util::getRandomFloat(-0.5f, 0.5f),7.5f,9.0f);

		PxRigidDynamic *sphereActor = PxCreateDynamic(*gPhysicsSDK, sphereTransform, sphereGeometry, *sphereMaterial, sphereDensity);
		if (!sphereActor)
		cerr<<"create actor failed!"<<endl;
		sphereActor->setAngularDamping(1.0f);
		sphereActor->setLinearDamping(1.0f);
		sphereActor->setMass(5.0f);
		gScene->addActor(*sphereActor);
		sphereActor->addForce(PxVec3(Util::getRandomFloat(-10.0f, 10.0f), 0.0f, Util::getRandomFloat(-40.0f, -30.0f)), PxForceMode::eIMPULSE);
		spheres.push_back(sphereActor);
	}
}

void ParticleGenerator::RenderActors()
{

	// Render all the actors in the scene & their shading
	for (int i=0;i<spheres.size();i++)
	{
		PxVec3 pos = spheres[i]->getGlobalPose().p;

		if (pos.y < -10.0f)
		{
			PxTransform sphereTransform(PxVec3(0.0f, 4.0f, 0.0f));
			sphereTransform.p  = PxVec3(-0.5f,7.5f,9.0f); //TODO: Positioning!
			spheres[i]->setGlobalPose(sphereTransform);
		}

		glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_diffuse_sphere);
		DrawActor(spheres[i]);
	}
}

// Convert transform matrix to pass it over to openGL.
void ParticleGenerator::getColumnMajor(PxMat33 m, PxVec3 t, float* mat)
{
	mat[0] = m.column0[0];
	mat[1] = m.column0[1];
	mat[2] = m.column0[2];
	mat[3] = 0;

	mat[4] = m.column1[0];
	mat[5] = m.column1[1];
	mat[6] = m.column1[2];
	mat[7] = 0;

	mat[8] = m.column2[0];
	mat[9] = m.column2[1];
	mat[10] = m.column2[2];
	mat[11] = 0;

	mat[12] = t[0];
	mat[13] = t[1];
	mat[14] = t[2];
	mat[15] = 1;
}