#include "stdafx.h"
/*************************************************************************** 
  PhysX ariables 
 ***************************************************************************/
ParticleGenerator* particleGenerator;
SceneBuilder* sceneBuilder;
std::vector<PxRigidDynamic*> boxActor;
std::vector<PxRigidDynamic*> actorCompound;

static PxPhysics* gPhysicsSDK = NULL;
static PxDefaultErrorCallback gDefaultErrorCallback;
static PxDefaultAllocator gDefaultAllocatorCallback;
static PxSimulationFilterShader gDefaultFilterShader=PxDefaultSimulationFilterShader;

float fps_max = 60.0f;

PxScene* gScene = NULL;
PxReal myTimestep = 1.0f/60.0f;

PxShape* aConvexShape;
PxRigidDynamic* convex;
char buffer [512];

//Particle Stuff
const PxU32 MAXPARTICLES = 1500;
const PxF32 PARTICLEDISTANCE = 0.25f;
PxParticleCreationData particleCreationData;
PxParticleFluid* particles;
PxU32 newAppParticleIndices[MAXPARTICLES];
PxVec3 particlePositionBuffer[MAXPARTICLES];
std::vector<PxU32>* releaseParticleBuffer;
std::vector<PxVec3>* releasedParticlePositionsBuffer;


/*************************************************************************** 
  Program Variables
 ***************************************************************************/
int startTime = 0, totalFrames = 0, state = 1, oldX = 0, oldY = 0, spawnTimer = 0, lastFrame = 0;
float fps = 0, rX = 0, rY = 50, dist = 0;
const int WINDOW_WIDTH = 1024, WINDOW_HEIGHT = 800;
const int FIELD_OF_VIEW_ANGLE = 45;
const float Z_NEAR = 1.0f, Z_FAR = 500.0f;

float g_rotation = 0;
float g_rotation_speed = 0.2f;

/*************************************************************************** 
  PhysX
 ***************************************************************************/
void StepPhysX()
{
    gScene->simulate(myTimestep);

	spawnTimer++;
		
	//if (spawnTimer > 10 && particleGenerator->spheres.size() < MAXPARTICLES)
	//{
	//	particleGenerator->CreateParticles(20);
	//	spawnTimer = 0;
	//}

    while(!gScene->fetchResults() )
    {
        // do something useful
    }
}

void SetOrthoForFont()
{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, WINDOW_WIDTH, 0, WINDOW_HEIGHT);
	glScalef(1, -1, 1);
	glTranslatef(0, -WINDOW_HEIGHT, 0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void ResetPerspectiveProjection()
{
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
}

void RenderSpacedBitmapString(int x, int y, int spacing, void *font, char *string)
{
	char *c;
	int x1=x;
	for (c=string; *c != '\0'; c++)
	{
		glRasterPos2i(x1,y);
		glutBitmapCharacter(font, *c);
		x1 = x1 + glutBitmapWidth(font,*c) + spacing;
	}
}


void InitializePhysX()
{
    /*******************************************************************************************/
    //INITIALIZE SCENE
    /*******************************************************************************************/
    PxFoundation* foundation = PxCreateFoundation(PX_PHYSICS_VERSION, gDefaultAllocatorCallback, gDefaultErrorCallback);

    gPhysicsSDK = PxCreatePhysics(PX_PHYSICS_VERSION, *foundation, PxTolerancesScale() );

    PxInitExtensions(*gPhysicsSDK);

    // Create the scene
    PxSceneDesc sceneDesc(gPhysicsSDK->getTolerancesScale());
    sceneDesc.gravity = PxVec3(0.0f, -9.8f, 0.0f);

    if(!sceneDesc.cpuDispatcher)
    {
        PxDefaultCpuDispatcher* mCpuDispatcher = PxDefaultCpuDispatcherCreate(1);
        sceneDesc.cpuDispatcher = mCpuDispatcher;
    }

    if(!sceneDesc.filterShader)
        sceneDesc.filterShader  = gDefaultFilterShader;

    gScene = gPhysicsSDK->createScene(sceneDesc);

    /*******************************************************************************************/
    //INITIALIZE ACTORS
    /*******************************************************************************************/

    //Particles
    PxMaterial* planeMaterial = gPhysicsSDK->createMaterial(0.9f, 0.1f, 0.5f);
	PxMaterial* cubeMaterial = gPhysicsSDK->createMaterial(0.3f, 0.3f, 0.01f);
	PxMaterial* sphereMaterial = gPhysicsSDK->createMaterial(0.3f, 0.3f, 0.01f);


    // Initialize FluidParticles
	PxU32 numberOfParticles = MAXPARTICLES;
	bool perParticleRestOffset = false;
	releaseParticleBuffer = new std::vector<PxU32>();
    releasedParticlePositionsBuffer = new std::vector<PxVec3>;

	particles = gPhysicsSDK->createParticleFluid(numberOfParticles, perParticleRestOffset);
	particles->setParticleBaseFlag(PxParticleBaseFlag::eCOLLISION_TWOWAY, true);

	particles->setRestParticleDistance(PARTICLEDISTANCE);
	if(particles)
	{
		gScene->addActor(*particles);
	}

	PxParticleCreationData particleCreationData;
	particleCreationData.numParticles = numberOfParticles;

	for(int i=0;i<MAXPARTICLES;i++)
	{
		newAppParticleIndices[i] = i;
	}
	particleCreationData.indexBuffer = PxStrideIterator<const PxU32>(newAppParticleIndices);
	
	PxVec3 newAppParticlePositions[MAXPARTICLES];
	PxU32 yj = 0;
	PxU32 zj = 0;
	for(int j=0;j<MAXPARTICLES;j++)
	{
		
			newAppParticlePositions[j].x = 0.0f-100;
			newAppParticlePositions[j].y = j * 0.005f;
			newAppParticlePositions[j].z = 1.9f;

	}
	particleCreationData.positionBuffer = PxStrideIterator<const PxVec3>(newAppParticlePositions);

	PxVec3 newAppParticleVelocity;
	newAppParticleVelocity.x = 20;
	newAppParticleVelocity.y = 0;
	newAppParticleVelocity.z = 0;
	particleCreationData.velocityBuffer = PxStrideIterator<const PxVec3>(&newAppParticleVelocity, 0);

	particles->createParticles(particleCreationData);
	particles->setParticleMass(0.02f);

	particles->setContactOffset(1.0f);
	particles->setViscosity(5.0f);

    // Create the Wheel with four Bretter and actors
	PxReal boxdensity = 0.5f;
	PxMaterial* mMaterial = gPhysicsSDK->createMaterial(0.5,0.5,0.5);
	
	std::vector<PxTransform> boxtransform;
	std::vector<PxBoxGeometry> boxgeom;			// We need 4 Bretter!!!

	PxQuat quat = PxQuat( 0.785398163, PxVec3(1.0f, 0.0f, 0.0f) );					// 45� rotation (in case we could use it)

	PxTransform boxtransform1(PxVec3(0.0f, 2.0, 1.5f), PxQuat::createIdentity());	// Unfortunately hard coded positions!!!
	PxTransform boxtransform2(PxVec3(0.0f, -1.0, -1.5f), PxQuat::createIdentity());
	PxTransform boxtransform3(PxVec3(0.0f, -4.0, 1.5f), PxQuat::createIdentity());
	boxtransform.push_back( boxtransform1 );	// Add position 4 times
	boxtransform.push_back( boxtransform2 );
	boxtransform.push_back( boxtransform3 );

	PxVec3 verDim = PxVec3(2.5f, 2.0f, 0.2f);	// vertical brett
	PxVec3 horDim = PxVec3(2.5f, 0.2f, 2.0f);	// horizontal brett
	boxgeom.push_back( verDim );				// Add both dimensions
	boxgeom.push_back( horDim );


	// Create an actor for each Brett and add it to the global boxActor
	/*for( int i = 0; i < 4; ++i )
	{
		PxRigidDynamic* actor = PxCreateDynamic( *gPhysicsSDK, boxtransform[i], boxgeom[i], *mMaterial, boxdensity );
		if( !actor )
		{
			cerr<<"create actor failed!"<<endl;
		}
		actor->setAngularDamping(0.75);
		actor->setLinearVelocity(PxVec3(0,0,3.0f));
		actor->setActorFlag(PxActorFlag::eDISABLE_GRAVITY, true);
		gScene->addActor(*actor);
		boxActor.push_back(actor);
	}*/

	for( int i = 0; i < 3; ++i )
	{
		actorCompound.push_back( PxCreateDynamic(*gPhysicsSDK, boxtransform[i], boxgeom[0], *mMaterial, boxdensity ) );

		actorCompound[i]->setAngularDamping(0.75);
		actorCompound[i]->setActorFlag(PxActorFlag::eDISABLE_GRAVITY, true);
		gScene->addActor(*actorCompound[i]);

		PxShape* shape2 = actorCompound[i]->createShape(boxgeom[1], *mMaterial);
	
		PxD6Joint* mJoint = PxD6JointCreate(*gPhysicsSDK, actorCompound[i], PxTransform::createIdentity(), nullptr, actorCompound[i]->getGlobalPose());
		mJoint->setMotion(PxD6Axis::eX,PxD6Motion::eLOCKED);
		mJoint->setMotion(PxD6Axis::eY,PxD6Motion::eLOCKED);
		mJoint->setMotion(PxD6Axis::eZ,PxD6Motion::eLOCKED);

		mJoint->setMotion(PxD6Axis::eSWING2,PxD6Motion::eLOCKED);
		mJoint->setMotion(PxD6Axis::eSWING1,PxD6Motion::eLOCKED);
		mJoint->setMotion(PxD6Axis::eTWIST,PxD6Motion::eFREE);
	}
}

void ShutdownPhysX()
{
    gScene->release();
    gPhysicsSDK->release();
}

void OnShutdown()
{
    ShutdownPhysX();
}

void getColumnMajor(PxMat33 m, PxVec3 t, float* mat)
{
	mat[0] = m.column0[0];
	mat[1] = m.column0[1];
	mat[2] = m.column0[2];
	mat[3] = 0;

	mat[4] = m.column1[0];
	mat[5] = m.column1[1];
	mat[6] = m.column1[2];
	mat[7] = 0;

	mat[8] = m.column2[0];
	mat[9] = m.column2[1];
	mat[10] = m.column2[2];
	mat[11] = 0;

	mat[12] = t[0];
	mat[13] = t[1];
	mat[14] = t[2];
	mat[15] = 1;
}

void DrawWheel(Model_OBJ obj)
{
 	glEnableClientState(GL_VERTEX_ARRAY);						// Enable vertex arrays
 	glEnableClientState(GL_NORMAL_ARRAY);						// Enable normal arrays
	glVertexPointer(3,GL_FLOAT,	0, obj.Faces_Triangles);				// Vertex Pointer to triangle array
	glNormalPointer(GL_FLOAT, 0, obj.normals);						// Normal pointer to normal array
	glDrawArrays(GL_TRIANGLES, 0, obj.TotalConnectedTriangles);		// Draw the triangles
	glDisableClientState(GL_VERTEX_ARRAY);						// Disable vertex arrays
	glDisableClientState(GL_NORMAL_ARRAY);						// Disable normal arrays
}

void DrawParticle(PxVec3 position)
{
	PxMat33 m;
	m = m.createIdentity();
	float mat[16];
	getColumnMajor(m, position, mat);
	glPushMatrix();
	glMultMatrixf(mat);
	glutSolidSphere(0.2f,3,2);
	glPopMatrix();
}

/*************************************************************************** 
  GLUT Render / Display Function
 ***************************************************************************/
void OnRender()
{
    //Get FrameCount
    totalFrames++;
    int current = glutGet(GLUT_ELAPSED_TIME);
    if((current-startTime)>1000)
    {
    float elapsedTime = float(current-startTime);
    fps = ((totalFrames * 1000.0f)/ elapsedTime);
    startTime = current;
    totalFrames = 0;    
    }
	float elapsedFrameTime = current - lastFrame;
	if(elapsedFrameTime < 1/fps_max*1000)
		Sleep(elapsedFrameTime - 1/fps_max*1000);

	//sprintf_s(buffer, " Particles: %i | FPS: %3.2f", particleGenerator->spheres.size(), fps);
	sprintf_s(buffer, "FPS: %3.2f | Rotate Scene by holding down the right mouse button.", fps);

    if (gScene)
        StepPhysX();

    glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);

	SetOrthoForFont();
	glColor3f(1,1,1);
	RenderSpacedBitmapString(20,20,0,GLUT_BITMAP_HELVETICA_12,buffer);

    glLoadIdentity();
    glTranslatef(0,0,0);
    gluLookAt(0, 1, 25, 0, 1, 0, 0, 1, 0);
	
	ResetPerspectiveProjection();
	glRotatef(rX,1,0,0);
    glRotatef(rY,0,1,0);

	for( int i = 0; i < actorCompound.size(); ++i )
	{
		sceneBuilder->DrawBoxActor(actorCompound[i]);
	}

    //Render Particles

    PxParticleFluidReadData* rd = particles->lockParticleFluidReadData();

    if (rd)
    {
            PxStrideIterator<const PxParticleFlags> flagsIt(rd->flagsBuffer);
            PxStrideIterator<const PxVec3> positionIt(rd->positionBuffer);

            for (unsigned i = 0; i < rd->validParticleRange; ++i, ++flagsIt, ++positionIt)
            {
                    if (*flagsIt & PxParticleFlag::eVALID)
                    {
                            // access particle position
                                const PxVec3& position = *positionIt;
                                if ( position.z > -5.9f && position.y > -10.0f)
                                {
                                    particlePositionBuffer[i] = position;
			                        DrawParticle(position);
                                }
                                else
                                {
                                    releaseParticleBuffer->push_back(i);
                                }
                    }
            }

            // return ownership of the buffers back to the SDK
            rd->unlock();
    }
    if(releaseParticleBuffer->size() > 0)
    {
        PxStrideIterator<const PxU32> indexit (&releaseParticleBuffer->at(0));
	    particles->releaseParticles(releaseParticleBuffer->size(), indexit);

        //Create Particles
		PxParticleCreationData particleCreationData;
		particleCreationData.numParticles = releaseParticleBuffer->size();
		particleCreationData.indexBuffer = indexit;

		releasedParticlePositionsBuffer->resize(releaseParticleBuffer->size());
		for(int q=0;q<releaseParticleBuffer->size();q++)
		{
			releasedParticlePositionsBuffer->at(q).x = 0;
			releasedParticlePositionsBuffer->at(q).y = 7.8f;
			releasedParticlePositionsBuffer->at(q).z = 8-(0.4f*q);
		}

		PxStrideIterator<const PxVec3> positionit(&releasedParticlePositionsBuffer->at(0));
		particleCreationData.positionBuffer = positionit;

		PxStrideIterator<const PxVec3> velocityit(&PxVec3(0, 0, -10), 0);
		particleCreationData.velocityBuffer = velocityit;
		particles->createParticles(particleCreationData);

		releaseParticleBuffer->clear();
		releasedParticlePositionsBuffer->clear();

    }

    
	// Render the rest
	sceneBuilder->RenderObjects();

    glutSwapBuffers();

}

void Mouse(int button, int s, int x, int y)
{
	if (s == GLUT_DOWN)
	{
		oldX = x;
		oldY = y;
	}

	if(button == GLUT_RIGHT_BUTTON)
	state = 1;
	else
	state = 0;
}

void Motion(int x, int y)
{
	if (state == 1)
	{
		rY += (x - oldX)/5.0f;
		rX += (y - oldY)/5.0f;
	}

	oldX = x;
	oldY = y;
	glutPostRedisplay();
}

/*************************************************************************** 
  GLUT Idle Function
 ***************************************************************************/
void OnIdle()
{
    glutPostRedisplay();
}

/*************************************************************************** 
  GLUT Initialization 
 ***************************************************************************/
void InitializeGlut()
{
    glMatrixMode(GL_PROJECTION);
	glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
	GLfloat aspect = (GLfloat) WINDOW_WIDTH / WINDOW_HEIGHT;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	gluPerspective(FIELD_OF_VIEW_ANGLE, aspect, Z_NEAR, Z_FAR);
    glMatrixMode(GL_MODELVIEW);
    glShadeModel( GL_SMOOTH );
    glClearColor(0.1, 0.1, 0.1, 0.0);
    glClearDepth( 1.0f );
    glEnable( GL_DEPTH_TEST );
    glDepthFunc( GL_LEQUAL );
    glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );
 
    GLfloat amb_light[] = { 0.5, 0.5, 0.5, 1.0 };
    GLfloat diffuse[] = { 0.6, 0.6, 0.6, 1 };
    GLfloat specular[] = { 0.7, 0.7, 0.3, 1 };
    glLightModelfv( GL_LIGHT_MODEL_AMBIENT, amb_light );
    glLightfv( GL_LIGHT0, GL_DIFFUSE, diffuse );
    glLightfv( GL_LIGHT0, GL_SPECULAR, specular );
    glEnable( GL_LIGHT0 );
    glEnable( GL_COLOR_MATERIAL );
    glShadeModel( GL_SMOOTH );
    glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE );
    glDepthFunc( GL_LEQUAL );
    glEnable( GL_DEPTH_TEST );
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0); 
}

void OnReshape(int nw, int nh)
{
glViewport(0,0,nw, nh);
glMatrixMode(GL_PROJECTION);
glLoadIdentity();
gluPerspective(50, (GLfloat)nw / (GLfloat)nh, 0.1f, 1000.0f);
glMatrixMode(GL_MODELVIEW);
}

void InitializeParticleGenerator()
{
	particleGenerator = new ParticleGenerator(gPhysicsSDK, gScene);
}

void InitializeSceneBuilder()
{
	sceneBuilder = new SceneBuilder(gPhysicsSDK, gScene);
}

/*************************************************************************** 
  CreateWheelPart Function
 ***************************************************************************/

PxRigidDynamic* createWheelpart(WavefrontObj wavefront)
{

	PxInitExtensions(*gPhysicsSDK);

	PxCooking *cooker = PxCreateCooking(PX_PHYSICS_VERSION,gPhysicsSDK->getFoundation(),PxCookingParams(PxTolerancesScale()));


	PxConvexMeshDesc convexDesc;
	convexDesc.points.count     = wavefront.mVertexCount;
	convexDesc.points.stride    = sizeof(PxVec3);
	convexDesc.points.data      = wavefront.mVertices;
	convexDesc.flags            = PxConvexFlag::eCOMPUTE_CONVEX;
	convexDesc.vertexLimit      = 256;

	PxDefaultMemoryOutputStream convexBuffer;
	if(!cooker->cookConvexMesh(convexDesc, convexBuffer))
		cout << "cooking convex error" << endl;
	PxDefaultMemoryInputData convexReadBuffer(convexBuffer.getData(), convexBuffer.getSize() );
	PxConvexMesh* convexMesh = gPhysicsSDK->createConvexMesh(convexReadBuffer);

	PxTriangleMeshDesc meshDesc;
	meshDesc.points.count				= wavefront.mVertexCount;
	meshDesc.triangles.count			= wavefront.mTriCount;
	meshDesc.points.stride				= sizeof(float) * 3;
	meshDesc.triangles.stride			= sizeof(int) * 3;
	meshDesc.points.data				= wavefront.mVertices;
	meshDesc.triangles.data				= wavefront.mIndices;

	
	PxDefaultMemoryOutputStream buffer;

	bool success = cooker->cookTriangleMesh(meshDesc, buffer); 

	if(!success)
		cout<<"Error cooking"<<endl;

	PxTriangleMesh* triangleMesh = NULL;
	PxDefaultMemoryInputData readBuffer(buffer.getData(), buffer.getSize() );
	triangleMesh = gPhysicsSDK->createTriangleMesh(readBuffer);

	PxMaterial* mMaterial = gPhysicsSDK->createMaterial(0.5,0.5,0.5);
	PxTransform transform(PxVec3(0.0f, 0.0f, 0.0f), PxQuat::createIdentity());	
	PxReal density = 1.0f;

	/*PxRigidStatic* rigidStatic = PxCreateStatic(*gPhysicsSDK, transform, PxTriangleMeshGeometry(triangleMesh), *mMaterial);

	if(!rigidStatic)
		cout<<"Failed creating actor"<<endl;*/
	PxRigidDynamic* meshActor;
	meshActor = gPhysicsSDK->createRigidDynamic(transform);
	PxShape* meshShape;

	meshActor->setRigidDynamicFlag(PxRigidDynamicFlag::eKINEMATIC, true);

	PxTriangleMeshGeometry triGeom;
	triGeom.triangleMesh = triangleMesh;
	meshShape = meshActor->createShape(triGeom, *mMaterial);
	gScene->addActor(*meshActor);

	meshActor->addTorque(PxVec3(0.0f, 0.0f, 0.0f), PxForceMode::eFORCE);

	PxShape* convexShape;
	PxConvexMeshGeometry convexGeom = PxConvexMeshGeometry(convexMesh);
	convexShape = meshActor->createShape(convexGeom, *mMaterial);				// We don't need the convex mesh. we use a triangle mesh
	convexShape->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);

	// ... now switch to dynamic

	meshShape->setFlag(PxShapeFlag::eSIMULATION_SHAPE, true);
	convexShape->setFlag(PxShapeFlag::eSIMULATION_SHAPE, true);
	meshActor->setRigidDynamicFlag(PxRigidDynamicFlag::eKINEMATIC, false);


	return meshActor;
}



/*************************************************************************** 
  Main Function 
 ***************************************************************************/

void main(int argc, char** argv)
{
    atexit(OnShutdown);
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );
    glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    glutCreateWindow("Waterwheel");

    glutDisplayFunc(OnRender);
    glutIdleFunc(OnIdle);//onIdle
	glutReshapeFunc(OnReshape);
	glutMouseFunc(Mouse);
	glutMotionFunc(Motion);
    //Initialization
    InitializePhysX();
    InitializeGlut();
	//InitializeParticleGenerator();
	InitializeSceneBuilder();
	

    
    
    // The global boxActor is used anywhere else.


	//meshActor->addTorque(PxVec3(5.0f, 0.0f, 0.0f), PxForceMode::eFORCE);
	
	//PxTransform trans = meshActor->getGlobalPose();
	//trans.rotate(PxVec3(10.0f, 50.0f, 10.0f));
	//meshActor->setGlobalPose(trans);

	//gScene->addActor(*rigidStatic);

	//Main Loop
    glutMainLoop();
}


